<?php

defined('ROOT_DIR') or define('ROOT_DIR', __DIR__);

require __DIR__ . '/vendor/autoload.php';
$config = require __DIR__ . '/app/config.php';


avtokar\lightmvc\bootstrap::run($config);