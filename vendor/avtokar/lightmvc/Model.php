<?php

namespace avtokar\lightmvc;

class Model extends \avtokar\Component
{
    protected $attributes = [];

    public function __get($name)
    {
        if (!empty($this->attributes[$name]))
            return $this->attributes[$name];

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->attributes))
            return $this->attributes[$name] = $value;

        return parent::__set($name, $value);
    }

    public function __construct()
    {
        $vars = get_object_vars($this);
        foreach ($vars as $attribute => $value) {
            if ($attribute == 'attributes' || $attribute == 'old_attributes')
                continue;
            $this->attributes[$attribute] = $value;
        }
    }
}