<?php

namespace avtokar\lightmvc;

class Application extends \avtokar\Component
{
    private $_db = null;
    private $_config = null;
    private $_post;
    private $_get;
    private $_session;
    private $_user;

    public function __construct($config)
    {
        session_start();
        $__get = [];
        $__post = [];
        $this->_session = (object)$_SESSION;
        $this->_config = (object)$config;
        if (!empty($_GET['controller'])) {
            $this->_config->controller = $_GET['controller'];
            unset ($_GET['controller']);
        };
        if (!empty($_GET['action'])) {
            $this->_config->action = $_GET['action'];
            unset ($_GET['action']);
        };
        if (!empty($_GET['ext'])) {
            $this->_config->ext = $_GET['ext'];
            unset ($_GET['ext']);
        };
        if(!empty($config['db'])) {
            $this->db;
        }

        $__post = $this->escape($_POST);
        $this->_post = (object)$__post;

        $__get = $this->escape($_GET);
        $this->_get = (object)$__get;
    }

    public function getDb()
    {
        if (!$this->_db)
            $this->_db = new Db($this->_config->db);
        return $this->_db;
    }

    public function getGet()
    {
        return $this->_get;
    }

    public function getPost()
    {
        return $this->_post;
    }

    public function getSession()
    {
        return $this->_session;
    }

    public function session($key, $value)
    {
        if (empty($key))
            return null;
        if (empty($value)) {
            unset($_SESSION[$key]);
            return null;
        };
        $_SESSION[$key] = $value;
        return $this->_session->$key = $value;
    }

    public function getIsGet()
    {
        return !empty($this->_get);
    }

    public function getIsPost()
    {
        return !empty($this->_post);
    }

    public function getConfig()
    {
        return $this->_config;
    }

    public function getUser()
    {
        return $this->_user;
    }

    public function setUser($user)
    {
        return $this->_user = $user;
    }

    private function escape($element)
    {
        $result = null;
        if (is_array($element)) {
            foreach ($element as $key => $value)
                $result[$key] = $this->escape($value);
        } else {
            $result = htmlentities($element);
        }
        return $result;
    }

    public function is_int($int)
    {
        if ("$int" === "" . intval($int))
            return intval($int);
        return null;
    }
}