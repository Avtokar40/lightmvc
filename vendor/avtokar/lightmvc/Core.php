<?php

namespace avtokar\lightmvc;

class Core
{
    static public $app = null;

    private function __construct()
    {
    }

    static function route()
    {
        if (!empty(Core::$app->session->userId)) {
            Core::$app->user = (new \app\model\Users())->findOne(Core::$app->session->userId);
        };
//        Core::$app->session('userId',1);
        $controller = ucfirst(strtolower(self::$app->config->controller)) . 'Controller';
        $action = 'action' . ucfirst(strtolower(self::$app->config->action));

        $controller = 'app\\controller\\' . $controller;
        echo (new $controller())->$action();
        return;
    }
}