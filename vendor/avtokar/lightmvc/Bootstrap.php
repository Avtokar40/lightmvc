<?php

namespace avtokar\lightmvc;


class Bootstrap
{
    static public function run($config = [])
    {
        Core::$app = new Application($config);
        Core::route();
        return null;
    }

}