<?php
/**
 * Created by PhpStorm.
 * User: Avtokar40
 * Date: 01.09.2018
 * Time: 22:39
 *
 * Всё остальное - искусство
 * В безумии быть собой
 */

namespace avtokar\lightmvc;


class View
{
    protected $layout = null;
    protected $viewdir = null;

    const ASSET_DIR = "/Assets/";
    const VIEWS_DIR = ROOT_DIR . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR;

    protected $css;
    protected $js;

    public function __construct($controller, $layout)
    {
        if ($layout)
            $this->layout = $layout;
        if ($controller)
            $this->viewdir = $controller;
        if (!empty(Core::$app->config->js))
            $this->js = Core::$app->config->js;
        if (!empty(Core::$app->config->css))
            $this->css = Core::$app->config->css;
    }

    public function render($view, $params)
    {
        $this->view = $view;

        if (!empty($params['js'])){
            foreach ($params['js'] as $file)
                $this->js[] = $file;
            unset($params['js']);
        };
        if (!empty($params['css'])){
            foreach ($params['css'] as $file)
                $this->css[] = $file;
            unset($params['css']);
        };

        if (!empty($params))
            extract($params);
        $css = "";
        foreach ($this->css as $file) {
            $css .= "<link href=\"" . $this->assets() . "css/$file.css\" rel=\"stylesheet\">\n";
        }
        $js = "";
        foreach ($this->js as $file) {
            $js .= "<script src=\"" . $this->assets() . "js/$file.js\"></script>\n";
        }

        ob_start();
        include $this->view($this->view);
        $content = ob_get_contents();
        ob_end_clean();

        if ($this->layout) {
            ob_start();
            include $this->layout();
            $content = ob_get_contents();
            ob_end_clean();

        }
        return $content;
    }

    private function layout()
    {
        return self::VIEWS_DIR .
            'layout' . DIRECTORY_SEPARATOR .
            "$this->layout.php";
    }

    private function view($view)
    {
        return self::VIEWS_DIR .
            "$this->viewdir" . DIRECTORY_SEPARATOR .
            "$view.php";
    }

    private function assets()
    {
        return self::ASSET_DIR;
    }
}