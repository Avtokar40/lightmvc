<?php

namespace avtokar\lightmvc;

class Db
{
    private $connect = null;
    private $_result = null;
    private $_query = null;

    public function __construct($params = [])
    {

        if (isset($params['host'])) {
            $host = $params['host'];
            unset($params['host']);
        };
        if (isset($params['dbase'])) {
            $dbase = $params['dbase'];
            unset($params['dbase']);
        };
        if (isset($params['user'])) {
            $user = $params['user'];
            unset($params['user']);
        };
        if (isset($params['pass'])) {
            $pass = $params['pass'];
            unset($params['pass']);
        };

        if ((!$host) && !empty($params))
            $host = array_shift($params);

        if ((!$dbase) && !empty($params))
            $dbase = array_shift($params);

        if ((!$user) && !empty($params))
            $user = array_shift($params);

        if ((!$pass) && !empty($params))
            $pass = array_shift($params);

        if (!(isset($host) && isset($dbase) && isset($user) && isset($pass)))
            return null;

        $this->connect = new \mysqli($host, $user, $pass, $dbase);
        return $this;
    }

    public function escape($string)
    {
        return $this->connect->escape_string($string);
    }

    public function columns($table)
    {
        $this->_query = "SHOW COLUMNS FROM `$table`";
        return $this->query()->fetch_all(MYSQLI_ASSOC);
    }

    public function keys($table)
    {
        $this->_query = "SHOW KEYS FROM `$table`";
        return $this->query()->fetch_all(MYSQLI_ASSOC);
    }

    public function select($params = [])
    {
        $fields = '*';
        $condition = "";
        $group = "";
        $order = "";
        $limit = "";
        if (empty($params) || empty($params['table']))
            return null;
        $table = $params['table'];

        if (!empty($params['fields'])) {
            $fields = $params['fields'];
            if (is_array($fields))
                $fields = implode(', ', $fields);
        };

        if (!empty($params['group'])) {
            $group = $params['group'];
            if (is_array($group)) {
                $order = implode(', ', $group);
            }
            $order = "ORDER BY " . $order;
        };

        if (!empty($params['order'])) {
            $order = $params['order'];
            if (is_array($order)) {
                $tmp = [];
                foreach ($order as $key => $value)
                    $tmp[] = "$key $value";
                $order = implode(', ', $tmp);
            }
            $order = "ORDER BY " . $order;
        };

        if (!empty($params['limit'])) {
            $limit = $params['limit'];
            if (is_array($limit)) {
                $offset = null;
                $count = null;
                if (isset($params['limit']['offset'])) {
                    $offset = $params['limit']['offset'];
                    unset($params['limit']['offset']);
                };
                if (isset($params['limit']['count'])) {
                    $count = $params['limit']['count'];
                    unset($params['limit']['count']);
                };

                if (!$offset && !empty($params['limit']))
                    $offset = array_shift($params['limit']);
                if (!$count && !empty($params['limit']))
                    $count = array_shift($params['limit']);

                $limit = $offset ?? '';
                $limit .= $count ?? '';
            }
            $limit = "LIMIT " . $limit;
        };

        if (!empty($params['condition'])) {
            $condition = $params['condition'];
            if (is_array($condition)) {
                $condition = $this->buildWhere($condition);
            };
            $condition = "WHERE " . $condition;
        };

        $this->_query = "SELECT $fields FROM $table $condition $group $order $limit";
        return $this->query();
    }

    public function put($table,$params, $insert)
    {
        $condition = "";
        if (!$insert) {
            $condition = "WHERE " . $params["PRIMARY_KEY"][0] . " = " . $params["PRIMARY_KEY"][1];
            unset ($params["PRIMARY_KEY"]);
        };
        $data = [];
        foreach ($params as $key => $value){
            if (!(is_int($value) || is_float($value)))
                $value = "'$value'";
            $data[] = "$key = $value";
        };
        $data = implode(', ',$data);
        $header = $insert?"INSERT INTO":"UPDATE";
        $this->_query = "$header $table SET $data $condition";
        return $this->query();
    }

    public function delete($table, $field, $value)
    {
        if (!(is_int($value) || is_float($value)))
            $value = "'$value'";
        $this->_query = "DELETE FROM $table WHERE $field = $value";
        return $this->query();
    }

    private function query($query = null)
    {
        if (!$query)
            $query = $this->_query;
        return $this->connect->query($query);
    }

    private function buildWhere($where)
    {
        $result = "";
        $action = array_shift($where);
        $first = array_shift($where);
        $second = array_shift($where);
        if ($action == 'and' || $action == "or") {
            $result = "( " . $this->buildWhere($first) . ") $action (" . $this->buildWhere($second) . ")";
        } else {
            if (!(is_int($second) || is_float($second)))
                $second = "'$second'";
            $result = "$first $action $second";
        }
        return $result;
    }
}