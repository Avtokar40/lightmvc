<?php

namespace avtokar\lightmvc;

class Controller extends \avtokar\Component
{
    protected $_layout;
    protected $_controller;

    public function __construct()
    {
        $this->_layout = Core::$app->config->layout;
        $class = static::class();
        $this->_controller = str_replace("controller", "", static::class());
    }

    public function setLayout($layout)
    {
        $this->_layout = $layout;
    }

    public function getLayout()
    {
        return $this->_layout;
    }

    public function render($_view = 'index', $params = [])
    {
        return (new View($this->_controller, $this->_layout))->render($_view, $params);
    }

    public function renderPartial($_view = 'index', $params = [])
    {
        return (new View(null, $this->_layout))->render($_view, $params);
    }

    protected function redirect($target = "")
    {
        $host = $_SERVER['HTTP_HOST'];
        header("Location: http://$host/$target");
        exit;
    }
}