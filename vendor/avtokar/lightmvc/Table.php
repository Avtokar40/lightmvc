<?php

namespace avtokar\lightmvc;

class Table extends Model
{
    protected $old_attributes = [];
    protected $keys;
    protected $params;

    static function tablename()
    {
        return static::class();
    }

    public function __set($name, $value)
    {
        if ($name === $this->keys['PRIMARY'])
            return false;
        return parent::__set($name, $value);
    }

    public function __construct()
    {
        foreach (core::$app->db->keys(static::tablename()) as $key) {
            $this->keys[$key['Key_name']] = $key['Column_name'];
        }
        foreach (core::$app->db->columns(static::tablename()) as $column) {
            $this->attributes[$column['Field']] = null;
            $this->old_attributes[$column['Field']] = null;
        }
    }

    public function fill($data = [])
    {
        try {
            if (isset($data[static::class()]) && is_array($data[static::class()]))
                $data = $data[static::class()];
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findOne($id)
    {
        $this->params = [
            'table' => static::tablename(),
            'condition' => ['=', $this->keys['PRIMARY'], $id],
            'limit' => '1',
        ];
        $res = core::$app->db->select($this->params)->fetch_assoc();

        if (empty($res))
            return null;

        foreach ($res as $key => $value) {
            $this->attributes[$key] = $value;
            $this->old_attributes[$key] = $value;
        }
        return $this;
    }

    public function find()
    {
        $this->params = [
            'table' => static::tablename(),
        ];
        return $this;
    }

    public function where($params)
    {
        $this->params['condition'] = $params;
        return $this;
    }

    public function one()
    {
        $this->params['limit']['count']="1";
        $res = core::$app->db->select($this->params)->fetch_assoc();
        if (empty($res))
            return null;
        foreach ($res as $key => $value) {
            $this->attributes[$key] = $value;
            $this->old_attributes[$key] = $value;
        }
        return $this;
    }

    public function all()
    {
        return core::$app->db->select($this->params)->fetch_all(MYSQLI_ASSOC);
    }

    public function limit($count, $offset=null)
    {
        $limit = ['count' => $count];
        if ($offset)
            $limit['offset'] = $offset;
        $this->params['limit'] = $limit;
        return $this;
    }

    public function count()
    {
        $params = [
            'table' => static::tablename(),
            'fields' => 'COUNT(*)',
        ];
        $res = core::$app->db->select($params)->fetch_array();
        return $res['COUNT(*)'];
    }

    public function save()
    {
        $insert = true;
        $fields = $this->attributes;
        if ($this->old_attributes[$this->keys['PRIMARY']]) {
            $insert = false;
            foreach ($fields as $key => $value) {
                if ($key === $this->keys['PRIMARY']) {
                    $fields['PRIMARY_KEY'] = [$key, $value];
                    continue;
                }
                if ($value === $this->old_attributes[$key])
                    unset($fields[$key]);
            }
        }
        $result = core::$app->db->put(static::tablename(),$fields,$insert);
        if($result){
            unset($fields['PRIMARY_KEY']);
            foreach ($fields as $key => $value){
                $this->old_attributes[$key]=$value;
            }
        }
        return $result;
    }

}