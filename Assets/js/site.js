$(function () {
    $("#login_form").submit(function (event) {
        event.preventDefault();
        //$.post( "test.php", { name: "John", time: "2pm" } );
        $.post("/user/login",
            $(this).serialize(),
            function (data) {
                var info = JSON.parse(data);
                console.log(info);
                if(info.result)
                    location.reload();
            });
    });
    $("#LogOut").click(function (event) {
        event.preventDefault();
        $.get("/user/logout",
            null,
            function (data) {
                var info = JSON.parse(data);
                console.log(info);
                if(info.result)
                    location.reload();
            });
    });
});