<?php

use app\model\Parser;

/** @var Parser $parser */
?>
<div class="row">
    <div class="col-xs-8">
        <form method="post">
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <label> Шаблон </label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control" type="text" name="template"
                           value="<?= !empty($parser->template) ? $parser->template : 'test template {here} {{andHere}} {{andAgainHere}} {anotherOne}' ?>">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <label> Текст для проверки </label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control" type="text" name="text"
                           value="<?= !empty($parser->text) ? $parser->text : 'test template OneOf Second again &lt;robot&gt;' ?>">
                </div>
            </div>
            <div class="col-xs-12">
                <input type="submit">
            </div>
        </form>
    </div>
    <div class="col-xs-8">
    <?php  if(!empty($parser->getResult())) :?>
    <pre>
        <?php var_dump($parser->result) ?>
    </pre>
        <?php endif;?>
    </div>
</div>