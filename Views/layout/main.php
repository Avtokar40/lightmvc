<?php

use avtokar\lightmvc\Core;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?? '' ?></title>
    <?= $js ?>
    <?= $css ?>
</head>
<body>
<header class="navbar-inverse">
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">LightMVC</a>
                </div>
                <!--                <div id="navbar" class="navbar-collapse collapse">-->
                <!---->
                <!--                    <ul class="nav navbar-nav navbar-right">-->
                <!--                        --><?php
                //                        $usr = \avtokar\lightmvc\Core::$app->user;
                //                        if (!empty($usr)): ?>
                <!--                            <div>-->
                <!--                                <span>-->
                <? //= \avtokar\lightmvc\Core::$app->user->name ?><!--</span>-->
                <!--                                <input type="button" value="LogOut" class="btn btn-default" id="LogOut">-->
                <!--                            </div>-->
                <!--                        --><? // else: ?>
                <!--                            <form class="form-inline" id="login_form">-->
                <!--                                <input type="text" class="form-control" placeholder="login" name="Users[login]">-->
                <!--                                <input type="password" class="form-control" placeholder="password"-->
                <!--                                       name="Users[password]">-->
                <!--                                <input type="submit" value="LogIn" class="btn btn-success">-->
                <!--                            </form>-->
                <!--                        --><? // endif; ?>
                <!--                    </ul>-->
                <!--                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
    </div>
</header>

<div class="wrap">
    <div class="container">
    <?php if (!empty(Core::$app->session->errorMessage)): ?>
        <div class="alert-danger alert fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?= Core::$app->session->errorMessage ?>
            <?php Core::$app->session('errorMessage', null); ?>
        </div>
    <?php endif; ?>

    <?php if (!empty(Core::$app->session->alertMessage)): ?>
        <div class="alert-warning alert fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?= Core::$app->session->alertMessage ?>
                <?php Core::$app->session('alertMessage', null); ?>
        </div>
    <?php endif; ?>

    <?php if (!empty(Core::$app->session->successMessage)): ?>
        <div class="alert-success alert fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?= Core::$app->session->successMessage ?>
                    <?php Core::$app->session('successMessage', null); ?>
        </div>
    <?php endif; ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Vasilchenko V.A. <?= date('Y') ?></p>

        <p class="pull-right">LightMVC</p>
    </div>
</footer>

</body>
</html>