<?php

namespace app\exception;

use Exception;
use Throwable;

class InvalidTemplateException extends Exception
{
    public function __construct($message = 'Invalid template.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}