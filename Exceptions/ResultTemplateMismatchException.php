<?php

namespace app\exception;

use Exception;
use Throwable;

class ResultTemplateMismatchException extends Exception
{
    public function __construct($message = 'Result not matches original template.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}