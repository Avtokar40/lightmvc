<?php

namespace app\controller;

use app\model\Parser;
use avtokar\lightmvc\Controller;
use avtokar\lightmvc\Core;
use Throwable;

class MainController extends Controller
{
    public function actionIndex()
    {
        try {
            $parser = new Parser();
            if(Core::$app->isPost) {
                $parser->template = Core::$app->post->template;
                $parser->check(Core::$app->post->text);
            }
        } catch (Throwable $e) {
            Core::$app->session->errorMessage = 'Исключение '. get_class($e). ': '. $e->getMessage();
        }
        return $this->render('index', ['parser' => $parser]);
    }
}