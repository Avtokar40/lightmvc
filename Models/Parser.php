<?php

namespace app\model;

use app\exception\InvalidTemplateException;
use app\exception\ResultTemplateMismatchException;

/**
 * Class Parser
 * @package app\model
 *
 * @property string template
 * @property string text
 */
class Parser
{
    protected $_template = '';
    protected $_text = '';
    protected $preparedTemplate = '';
    protected $_tokens = ['{', '}'];
    protected $matches = ['pure' => [], 'escape' => []];
    protected $_result;

    public function __get($name)
    {
        $name = ucfirst(strtolower($name));
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (method_exists($this, 'set' . $name)) {
            throw new \Exception('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } elseif (method_exists($this, 'get' . $name)) {
            throw new \Exception('Setting read-only property: ' . get_class($this) . '::' . $name);
        } else {
            throw new \Exception('Setting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function setTemplate($template)
    {
        $this->_template = $template;
        $this->preparedTemplate = $this->checkTemplate($template);
    }

    public function getTemplate()
    {
        return $this->_template;
    }

    public function getText()
    {
        return $this->_text;
    }

    public function getResult()
    {
        return $this->_result;
    }

    public function setText($text)
    {
        $this->_text = $text;
    }

    public function check($string)
    {
        $this->text = $string;
        $result = [];
        $pattern = '#^' . $this->preparedTemplate . '$#';
        if (false == preg_match_all($pattern, $this->text, $matches, PREG_PATTERN_ORDER)) {
            throw new ResultTemplateMismatchException();
        }
        foreach ($matches as $key => $item) {
            if (!empty($this->matches['pure'][$key])) {
                $result[$this->matches['pure'][$key]] = $item[0];
            }
            if (!empty($this->matches['escape'][$key])) {
                $result[$this->matches['escape'][$key]] = html_entity_decode($item[0]);
            }
        }
        $this->_result = $result;
        return $this->_result;
    }

    protected function checkTemplate($template)
    {
        $matches = &$this->matches;
        $leftToken = preg_quote($this->_tokens[0]);
        $rightToken = preg_quote($this->_tokens[1]);
        $template = preg_replace_callback_array([
            '#' . $leftToken . $leftToken . '\s*(\w+)\s*' . $rightToken . $rightToken . '#' => function ($match) use (&$matches) {
                $index = 'p' . count($matches['pure']);
                $result = '(?<' . $index . '>\w+)';
                $matches['pure'][$index] = $match[1];
                return $result;
            }, '#' . $leftToken . '\s*(\w+)\s*' . $rightToken . '#' => function ($match) use (&$matches) {
                $index = 'e' . count($matches['escape']);
                $result = '(?<' . $index . '>\S+?)';
                $matches['escape'][$index] = $match[1];
                return $result;
            }], $template);
        $tokens = '#[' . $leftToken . $rightToken . ']#';
        if (!empty(preg_grep($tokens, [$template]))) {
            throw new InvalidTemplateException();
        }
        return $template;
    }

}