<?php
return [
//    'db' => require __DIR__ . '/config/db.php',
    'controller' => 'main',
    'action' => 'index',
    'layout' => 'main',
    'js' => [
        'jquery-3.3.1.min',
        'bootstrap',
        'site',
    ],
    'css' => [
        'bootstrap',
        'site',
    ],
];